var global = {};

var files = [
  "fur-elise.mid",
  "entertainer.mid"
];

function makeMIDIRow(filename) {
	var prefix = "./files/";
	var listItem = document.createElement("ul");
	var a = document.createElement("input");
	a.type = "button";
	a.value = "Play " + filename;
	a.href = "#";
	a.onclick = function() {
		MIDIjs.stop();
		MIDIjs.play(prefix + filename);
	}
	listItem.appendChild(a);
	global.list.appendChild(listItem);
}

function displayStatusText(text) {
	global.status.textContent = text;
}

function showMIDIStatus(text) {
	displayStatusText(text);
}

function showMIDIStatusEvent(evt) {
	displayStatusText("Playing " + evt.time.toFixed(1) + " seconds.");
}

function setGlobals () {
	global.list = document.getElementById("dynamic-list");
	global.status = document.getElementById("status");
}

function makeMIDIRows() {
	for (var filename of files) {
		makeMIDIRow(filename);
	}
}

function onContentLoaded() {
	setGlobals();
	displayStatusText('loaded.');
	MIDIjs.message_callback = showMIDIStatus;
	MIDIjs.player_callback = showMIDIStatusEvent;
	makeMIDIRows();
}

function loadChecker() {
	if (document.readyState === 'complete') {
		clearInterval(readyStateCheckInterval);
		onContentLoaded();
	}
}

var readyStateCheckInterval = window.setInterval(loadChecker, 100);